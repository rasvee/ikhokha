package com.ikhokha.techcheck;


import com.ikhokha.techcheck.services.Reports;

import java.io.File;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Future;

public class Main {

	public static void main(String[] args) {

		Reports reports = new Reports();
		Set<Future<Map<String, Integer>>> fileResults = new HashSet<>();
		Map<String, Integer> totalResults = new HashMap<>();
		File docPath = new File("docs");
		File[] commentFiles = docPath.listFiles((d, n) -> n.endsWith(".txt"));

		reports.generateReport(commentFiles,fileResults);
		reports.getFileReport(fileResults, totalResults);

		System.out.println("RESULTS\n=============");
		totalResults.forEach((k,v) -> System.out.println(k + " : " + v));
	}
	

}


