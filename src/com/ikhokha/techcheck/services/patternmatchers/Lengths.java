package com.ikhokha.techcheck.services.patternmatchers;

import com.ikhokha.techcheck.services.PatternMatcher;

public class Lengths implements PatternMatcher {

    public static enum LengthCondition {
        GreaterThan,
        LessThan,
        Equal
    }

    private String mention;
    private int length;
    private LengthCondition lengthCondition;
/**
   Lengths  Constructors to set the initial state of an object
 */
    public Lengths(String mention, LengthCondition lengthCondition, int length){
        this.mention = mention;
        this.length = length;
        this.lengthCondition = lengthCondition;
    }

    @Override
    public String getReportKey() {
        return this.mention;
    }
    /**
     Method to count comments which are less,equal or greater than 15
     */
    @Override
    public int count(String comment) {
        switch (this.lengthCondition){
            case Equal:
                return comment.length() == length ? 1 : 0;
            case LessThan:
                return comment.length() < length ? 1 : 0;
            case GreaterThan:
                return comment.length() > length ? 1 : 0;
        }
        return 0;
    }
}
