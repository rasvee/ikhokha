package com.ikhokha.techcheck.services.patternmatchers;

import com.ikhokha.techcheck.services.PatternMatcher;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Occurrences implements PatternMatcher {

    private String mention;
    private String keyword;

    /**
     Contains Constructors to set the initial state of an object
     */
    public Occurrences(String mention, String keyword) {
        this.mention = mention;
        this.keyword = keyword;
    }

    @Override
    public String getReportKey() {
        return this.mention;
    }
    /**
     Method to count number of times regex is found in comments
     */
    @Override
    public int count(String comment) {
        Pattern pattern = Pattern.compile(keyword, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(comment);
        int count = 0;
        while (matcher.find()) {
            count++;
        }
        return count;
    }
}
