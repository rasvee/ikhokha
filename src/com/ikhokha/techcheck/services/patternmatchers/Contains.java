package com.ikhokha.techcheck.services.patternmatchers;

import com.ikhokha.techcheck.services.PatternMatcher;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Contains implements PatternMatcher {

    private String mention;
    private String keyword;
    /**
     Contains Constructors to set the initial state of an object
     */
    public Contains(String mention, String keyword){
        this.mention = mention;
        this.keyword= keyword;
    }

    @Override
    public String getReportKey() {
        return mention;
    }
    /**
    Method to count number of times regex is found in comments
     */
    @Override
    public int count(String comment) {
        Pattern pattern = Pattern.compile(keyword, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(comment);
        return matcher.find() ? 1 : 0;
    }

}
