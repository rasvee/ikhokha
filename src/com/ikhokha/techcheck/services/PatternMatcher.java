package com.ikhokha.techcheck.services;

public interface PatternMatcher {
    String getReportKey();
    int count(String comment);
}