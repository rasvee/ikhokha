package com.ikhokha.techcheck.services;

import com.ikhokha.techcheck.services.patternmatchers.Contains;
import com.ikhokha.techcheck.services.patternmatchers.Lengths;
import com.ikhokha.techcheck.services.patternmatchers.Occurrences;

import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.*;

public class Reports {

    private static final int THREAD_IN_POOL = 4;

    public void generateReport(File[] files, Set<Future<Map<String, Integer>>> results){
        ExecutorService service = Executors.newFixedThreadPool(THREAD_IN_POOL);
        try {
            List<PatternMatcher> matchers = createReportMatcher();
            for (File commentFile : files) {
                Callable<Map<String, Integer>> callable = new CommentAnalyzer(commentFile, matchers);
                Future<Map<String, Integer>> future = service.submit(callable);
                results.add(future);
            }
        }catch (Exception e){
            e.printStackTrace();
            System.out.println("Cannot generate report: " + e.getMessage());
        }
    }

    /**
      Get report for each comment file
     */
  public void getFileReport(Set<Future<Map<String, Integer>>> fileResults, Map<String, Integer> totalResults){
        for (Future<Map<String, Integer>> future : fileResults) {
            try {
                addReportResults(future.get(), totalResults);
            } catch (InterruptedException e) {
                e.printStackTrace();
                System.out.println("InterruptedException: " + e.getMessage());
            } catch (ExecutionException e) {
                e.printStackTrace();
                System.out.println("ExecutionException: " + e.getMessage());
            } catch (Exception e){
                e.printStackTrace();
                System.out.println(" Error generating report: " + e.getMessage());
            }
        }
    }

    /**
     List the list of matchers
     */
    private List<PatternMatcher> createReportMatcher() throws Exception{
        final String URL_REGEX = "((http:\\/\\/|https:\\/\\/)?(www.)?(([a-zA-Z0-9-]){2,}\\.){1,4}([a-zA-Z]){2,6}(\\/([a-zA-Z-_\\/\\.0-9#:?=&;,]*)?)?)";
        List<PatternMatcher> matchers = Arrays.asList(
                new Occurrences("NUMBER OF TIMES MOVER_MENTIONED", "Mover"),
                new Occurrences("NUMBER OF TIMES SHAKER_MENTIONED", "Shaker"),
                new Lengths("COMMENTS SHORTER_THAN_15", Lengths.LengthCondition.LessThan, 15),
                new Contains("SPAM FOUND", URL_REGEX),
                new Contains("QUESTIONS FOUND", "[?]")
        );
        return matchers;
    }

    /**
      Adds the result counts from a source map to the target map
     */
    private static void addReportResults(Map<String, Integer> source, Map<String, Integer> target) {
        for (Map.Entry<String, Integer> entry : source.entrySet()) {
            target.putIfAbsent(entry.getKey(), 0);
            target.put(entry.getKey(), target.get(entry.getKey()) + entry.getValue());
        }
    }
}
